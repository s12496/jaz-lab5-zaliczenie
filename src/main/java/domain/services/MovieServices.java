package domain.services;

import java.util.ArrayList;
import java.util.List;

import domain.Comment;
import domain.Movie;

public class MovieServices {
	
	private static List<Movie> db = new ArrayList<Movie>();
	private static int currentId = 1;
	private static int currentCommentId = 1;
	
	public List<Movie> getAll( ) {
		return db;
	}
	
	public Movie get(int id) {
		for(Movie m : db) {
			if (m.getId() == id) {
				return m;
			}
		}
		return null;
	}
	
	public void add(Movie m) {
		m.setId(++currentId);
		db.add(m);
	}

	public void update(Movie movie) {
		for(Movie m : db) {
			if(m.getId() == movie.getId()) {
				m.setTitle(movie.getTitle());
			}
		}
	}

	
	public void addComment( int movieId, Comment comment ) {
        Movie movie = get( movieId );
        comment.setId( currentId );
        setCurrentCommentId(getCurrentCommentId() + 1);
        movie.getComments().add(comment);
    }
	
	public static int getCurrentCommentId() {
		return currentCommentId;
	}

	public static void setCurrentCommentId(int currentCommentId) {
		MovieServices.currentCommentId = currentCommentId;
	}
}